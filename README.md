GL Python Tasks for DevOps ProCamp
==============================

*The `src/` directory in this project contains my solutions and unit tests to some of tasks below.*


<br>
<br>


---
Task 1 - "Shwalengthimeter"
---
You are given with a list with strings.
You need to return list with modified strings:

- first character is removed
- next character is removed if it is vowel("a", "e", "o", "i", "u", "y")
- added "shwa" to the beginning
- added " " (space) to the end
- added length of original string to the end

> **Example:**
- apple -> shwapple 5
- banana ->  shwanana 6
- tractor ->  shwaractor 7

>In:
```["garik", "balkon"]```

>Out:
```["shwarik 5", "shwalkon 6"]```


<br>
<br>


---
Task 2 - "String invertion"
---

Write three methods of string invertion.

> `s = "Invert me please"`

You need to get "esaelp em trevnI" in three ways


<br>
<br>


---
Task 3 - "Parts of list"
---
Write a function that gives all the ways to divide a list of at least two elements in two non-empty parts.

* Each two non empty parts will be in a tuple
* Each part will be in a string
* Elements of a pair must be in the same order as in the original array.

> **Example:**

> In:

> `a = ["az", "toto", "picaro", "zone", "kiwi"]`

> `partlist(a)`

> Out:

> `[('az', 'toto picaro zone kiwi'), ('az toto', 'picaro zone kiwi'), ('az toto picaro', 'zone kiwi'), ('az toto picaro zone', 'kiwi')]`


<br>
<br>


---
Task 4 - "Multiplication table"
---
Show your skills at multiplication (and string formatting too)!

Write some code that will return nice table-form multiplication table like this one:
> **Example:**
> ```
   1   2   3   4   5   6   7   8   9  10
   2   4   6   8  10  12  14  16  18  20
   3   6   9  12  15  18  21  24  27  30
   4   8  12  16  20  24  28  32  36  40
   5  10  15  20  25  30  35  40  45  50
   6  12  18  24  30  36  42  48  54  60
   7  14  21  28  35  42  49  56  63  70
   8  16  24  32  40  48  56  64  72  80
   9  18  27  36  45  54  63  72  81  90
  10  20  30  40  50  60  70  80  90 100
```

Try to use as few lines as possible.


<br>
<br>


---
Task 5 - "Anagram detection"
---

An anagram is the result of rearranging the letters of a word to produce a new word. Anagrams are case insensitive.

> **Examples:**

> `foefet` is an anagram of `toffee`

> `Buckethead` is an anagram of `DeathCubeK`

Write a program which **return True/False** based on are two passed string args are anagrams or not.


<br>
<br>


---
Task 6 - "Finding lambda"
---

Find the anonymous function in the given array and use the function to process the rest members of array (like map does)
> **Example:**

> In: ```find_function([lambda a: a+2,9,3,1,0])```

> Out: ```[11,5,3,2]```


> In: ```find_function([9,3,lambda a: a/2.0,1,0])```

> Out: ```[4.5,1.5,0.5,0.0]```

Итого: есть лист, в нём один из элементов (неизвестно какой именно) - **lambda** функция.
Её надо найти и использовать на всех остальных элементах списка.


<br>
<br>


---
Task 7 - "Recursive function"
---

Write recursive function which will calculate sum of all digits in this list:
> **Example:**

> ```[[[ [1, 4, 5], [[6, 9],[[[8, 1], 7], 3], 2], 7], 5, 2], 9, [1, 2]]```

Additional task:
- Write the same using string processing


<br>
<br>


---
Task 8 - "Group anagrams"
---

Process all words from input sequence and return a list with lists from those words that are anagrams with others in their list.

A typical test could be :
> **Example:**

> In: ```groupAnagrams(["tsar", "rat", "tar", "star", "tars", "cheese"])```

> Out: ```[["tsar", "star", "tars"],["rat", "tar"],["cheese"]]```


<br>
<br>


---
Task 9 - "Dj helper tool"
---

Create `get_song()` helper function that takes two parameters - dictionary with songs (see below) and integer argument which is a maximum length of a song in seconds.

Songs is an array of objects which are formatted as follows:
> **Example:**

> ```{'artist': 'Artist', 'title': 'Title String', 'playback': '04:30'}```

You can expect playback value to be formatted exactly like above.

Result should be a title of the longest song from the database that matches the criteria of not being longer than specified time.
If there's no songs matching criteria in the database, return False.


<br>
<br>


---
Task 10 - "Narcissistic number"
---

A Narcissistic Number is a number which is the sum of its own digits, each raised to the power of the number of digits.

> **Example:**

> take 153 (3 digits): `1^3 + 5^3 + 3^3 = 1 + 125 + 27 = 153`

> take 1634 (4 digits): `1^4 + 6^4 + 3^4 + 4^4 = 1 + 1296 + 81 + 256 = 1634`

The Challenge:
Your code must return **true** or **false** depending upon whether the given number is a Narcissistic number.
> **Example:**

> `narcissistic(7)  # True`

> `narcissistic(371) # True`

> `narcissistic(122) # False`

> `narcissistic(4887) # False`


<br>
<br>

---
Task 11 - "Battery capacity"
---

Task is to return the value of left battery capacity parsed from given string (let's assume it is some shell command's result).

Current level should be calculated as **CurrentCapacity / MaxCapacity** in percents.

The resulting value should be like the following.
> **Example:**

> `61.41%`


<br>
<br>

---
Task 12 - "Merging logs"
---

You have two list of dictionaries of this format:

> **Example:**

> `{"id": "123123123123123", "message": "Some logged message", "datetime": 1474624791}`


You need to merge two list removing possible duplicates (judging by id key of dicts)

And additionally - sort resulted list by datetime timestamp (UNIX seconds).
