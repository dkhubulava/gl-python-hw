#!/usr/bin/python3
# -*- coding: utf-8 -*-

import unittest
from task11_battery_capacity import get_battery_level


class TestMeTestCase(unittest.TestCase):

    def test_1(self):
        self.assertEqual(
            get_battery_level('''"MaxCapacity" = 4540
                                 "CurrentCapacity" = 2897
                                 "LegacyBatteryInfo" = {"Amperage"=18446744073709550521,
                                                        "Flags"=4,"Capacity"=4540,
                                                        "Current"=2897,
                                                        "Voltage"=7283,
                                                        "Cycle Count"=406}
                                  "DesignCapacity" = 6700'''), '63.81%')

    def test_2(self):
        self.assertEqual(
            get_battery_level('''"MaxCapacity" = 7900
                                 "CurrentCapacity" = 3456
                                 "LegacyBatteryInfo" = {"Amperage"=18446744073709550521,
                                                        "Flags"=4,"Capacity"=7900,
                                                        "Current"=3456,
                                                        "Voltage"=7283,
                                                        "Cycle Count"=406}
                                  "DesignCapacity" = 6700'''), '43.75%')

    def test_3(self):
        self.assertEqual(
            get_battery_level('''"MaxCapacity" = 12000
                                 "CurrentCapacity" = 6542
                                 "LegacyBatteryInfo" = {"Amperage"=18446744073709550521,
                                                        "Flags"=4,"Capacity"=12000,
                                                        "Current"=6542,
                                                        "Voltage"=7283,
                                                        "Cycle Count"=406}
                                  "DesignCapacity" = 6700'''), '54.52%')
