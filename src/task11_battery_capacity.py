#!/usr/bin/python3

"""
Task is to return the value of left battery capacity
parsed from given string
(let's assume it is some shell command's result).

Current level should be calculated as
CurrentCapacity / MaxCapacity in percents.
The resulting value should be like the following example:

- 61.41%
"""

BATERY_INFO = '''"MaxCapacity" = 4540
                 "CurrentCapacity" = 2897
                 "LegacyBatteryInfo" = {"Amperage"=18446744073709550521,
                                        "Flags"=4,
                                        "Capacity"=4540,
                                        "Current"=2897,
                                        "Voltage"=7283,
                                        "Cycle Count"=406}
                 "DesignCapacity" = 6700'''


def get_battery_level(_data):
    """Return the value of left battery capacity
    parsed from given string"""
    _data = _data.split()
    current_level = int(_data[5]) / int(_data[2])

    return "{:.2%}".format(current_level)


print(get_battery_level(BATERY_INFO))
