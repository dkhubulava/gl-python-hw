#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Task:

- Process all words from input sequence and return a list
with lists from those words that are anagrams with others in their list

A typical test could be :

// input
groupAnagrams(["tsar", "rat", "tar", "star", "tars", "cheese"]);

// output [["tsar", "star", "tars"],
           ["rat", "tar"],
           ["cheese"]]
...

"""

test_list = ["tsar", "rat", "tar", "star", "tars", "cheese"]


def is_anagram(test, original):
    """Function will return True/False based on
     are two passed string args are anagrams or not."""

    test = list(test.lower())
    original = list(original.lower())

    test.sort()
    original.sort()

    index = 0
    is_equal = True

    if len(test) != len(original):
        return False

    while index < len(test) and is_equal:
        if test[index] == original[index]:
            index += 1
        else:
            is_equal = False

    return is_equal


def group_anagrams(input_list):
    result = []
    for i in input_list:
        tmp_list = []

        for j in input_list:
            if is_anagram(j, i):
                tmp_list.append(j)
        if tmp_list not in result:
            result.append(tmp_list)

    return result


print(group_anagrams(test_list))
