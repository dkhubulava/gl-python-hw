#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Write three methods of string invertion.
s = "Invert me please"
(You need to get "esaelp em trevnI" in three ways)
"""

s = "Invert me please"

# 1st way
print(s[::-1])

# 2nd way
print(''.join(reversed(s)))

# 3rd way
for i in reversed(s):
    print(i, end='')
print()
