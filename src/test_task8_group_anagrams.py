#!/usr/bin/python3
# -*- coding: utf-8 -*-

import unittest
from task8_group_anagrams import group_anagrams


class MyTestCase(unittest.TestCase):

    def test_1(self):
        self.assertEqual(group_anagrams(["tsar", "rat", "tar", "star", "tars", "cheese"]),
                                        [["tsar", "star", "tars"], ["rat", "tar"], ["cheese"]])

    def test_2(self):
        self.assertEqual(group_anagrams(["Aa", "bb", "aa", "BB", "Bb"]),
                                        [["Aa", "aa"], ["bb", "BB", "Bb"]])

    def test_3(self):
        self.assertEqual(group_anagrams(["AaAa", "Baaa", "aaaa", "bbbb"]),
                                        [["AaAa", "aaaa"], ["Baaa"], ["bbbb"]])
