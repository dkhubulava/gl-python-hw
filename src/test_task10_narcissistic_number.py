#!/usr/bin/python3
# -*- coding: utf-8 -*-

import unittest
from task10_narcissistic_number import narcissistic


class TestMeTestCase(unittest.TestCase):

    def test_1(self):
        self.assertEqual(narcissistic(7), True)

    def test_2(self):
        self.assertEqual(narcissistic(371), True)

    def test_3(self):
        self.assertEqual(narcissistic(122), False)

    def test_4(self):
        self.assertEqual(narcissistic(4887), False)
