#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Write recursive function which will calculate sum
of all digits in this list:

[[[ [1, 4, 5], [[6, 9],[[[8, 1], 7], 3], 2], 7], 5, 2], 9, [1, 2]]


Additional task:
- Write the same using string processing
"""

list_1 = [1, 4, 5, 6, 9, 8, 1, 7, 3, 2, 7, 5, 2, 9, 1, 2]
list_2 = [[[[[[[[[[[1, 11, 111]]]]]]]]]]]


def test_me(list_):
    sum = 0
    res_list = []

    def to_list(list_):
        for item in list_:
            if isinstance(item, list):
                to_list(item)
            else:
                res_list.append(item)

    to_list(list_)

    for i in res_list:
        sum += i

    return sum


print(test_me(list_1))
print(test_me(list_2))
