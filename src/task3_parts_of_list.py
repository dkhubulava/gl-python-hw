#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Task:

Write a function that gives all the ways
to divide a list of at least two elements in two non-empty parts.

    Each two non empty parts will be in a tuple
    Each part will be in a string
    Elements of a pair must be in the same order as in the original array.

Example:

>>> a = ["az", "toto", "picaro", "zone", "kiwi"]
>>> partlist(a)

[('az', 'toto picaro zone kiwi'),
 ('az toto', 'picaro zone kiwi'),
 ('az toto picaro', 'zone kiwi'),
 ('az toto picaro zone', 'kiwi')]
"""

list_ = ["az", "toto", "picaro", "zone", "kiwi"]


def partlist(list_):
    res_list = []
    tmp_list = []

    index = 1

    while index < len(list_):
        tmp_list.append(" ".join(list_[:index]))
        tmp_list.append(" ".join(list_[index:]))
        index += 1

    for i in zip(* [iter(tmp_list)] * 2):
        res_list.append(i)

    return res_list


print(partlist(list_))
