#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
An anagram is the result of rearranging the letters
of a word to produce a new word. Anagrams are case insensitive

Examples:
- foefet is an anagram of toffee
- Buckethead is an anagram of DeathCubeK

Task:
Write a program which return True/False
based on are two passed string args are anagrams or not.
"""


def is_anagram(test, original):
    """Function will return True/False based on
    are two passed string args are anagrams or not."""

    test = list(test.lower())
    test.sort()
    original = list(original.lower())
    original.sort()

    index = 0
    is_equal = True

    if len(test) != len(original):
        return False

    while index < len(test) and is_equal:
        if test[index] == original[index]:
            index += 1
        else:
            is_equal = False

    return is_equal


print(is_anagram("Buckethead", "DeathCubeK"))
