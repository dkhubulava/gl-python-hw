#!/usr/bin/python3
# -*- coding: utf-8 -*-

import unittest
from task9_dj_helper_tool import get_song
from task9_dj_helper_tool import songs_db


class TestMeTestCase(unittest.TestCase):

    def test_1(self):
        self.assertEqual(get_song(songs_db, 145), "Best possible song: Stepan Galyabarda/Lyst do mamy")

    def test_2(self):
        self.assertEqual(get_song(songs_db, 195), "Best possible song: Nirvana/The Man who sold the world")

    def test_3(self):
        self.assertEqual(get_song(songs_db, 0), False)
