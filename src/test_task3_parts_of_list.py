#!/usr/bin/python3
# -*- coding: utf-8 -*-

import unittest
from task3_parts_of_list import partlist


class MyTest(unittest.TestCase):

    def test1(self):
        self.assertEqual(partlist(['1', '2', '3']),
                                  [('1', '2 3'), ('1 2', '3')])

    def test2(self):
        self.assertEqual(partlist(["az", "toto", "picaro", "zone", "kiwi"]),
                                  [('az', 'toto picaro zone kiwi'),
                                   ('az toto', 'picaro zone kiwi'),
                                   ('az toto picaro', 'zone kiwi'),
                                   ('az toto picaro zone', 'kiwi')])
