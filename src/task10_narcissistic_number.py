#!/usr/bin/python3

"""
A Narcissistic Number is a number which is the sum of its own digits,
each raised to the power of the number of digits.

For example, take 153 (3 digits):

- 1^3 + 5^3 + 3^3 = 1 + 125 + 27 = 153

and 1634 (4 digits):

- 1^4 + 6^4 + 3^4 + 4^4 = 1 + 1296 + 81 + 256 = 1634

The Challenge:
Your code must return true or false depending upon
whether the given number is a Narcissistic number.

- narcissistic(7)  # True,
- narcissistic(371) # True
- narcissistic(122) # False
- narcissistic(4887) # False
"""

import math


def narcissistic(test_number):
    num_of_digits = int(math.log10(test_number)) + 1
    sum_of_digits = sum([int(i) ** num_of_digits for i in list(str(test_number))])
    return test_number == sum_of_digits


print(narcissistic(7))  # True,
print(narcissistic(371))  # True
print(narcissistic(122))  # False
print(narcissistic(4887))  # False
