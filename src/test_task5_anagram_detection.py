#!/usr/bin/python3
# -*- coding: utf-8 -*-

import unittest
from task5_anagram_detection import is_anagram


class MyTest(unittest.TestCase):

    def test1(self):
        self.assertFalse(is_anagram("Good", "GooOD"))

    def test2(self):
        self.assertTrue(is_anagram("foefet", "toffee"))

    def test3(self):
        self.assertTrue(is_anagram("Buckethead", "DeathCubeK"))
